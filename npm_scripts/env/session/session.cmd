@echo off

for %%i in ("%~dp0..\..") DO (set dirVar=%%~ni)
for %%i in ("%~dp0.") DO (set dir2Var=%%~ni)

set finalValue=%dirVar% %dir2Var%
set kitty=%CD%\npm_scripts\env\session\kitty\kitty_portable.exe

start C:\"Program Files"\ConEmu\ConEmu64.exe -cmdlist ^
%kitty% -load 192.168.33.10 -cmd "cd /var/www/webdev.local && clear && npm run services:start" -cur_console:fna:t:"%finalValue%":C:"C:\cygwin64\Cygwin.ico" ^|^|^| ^
%kitty% -load 192.168.33.10 -cmd "cd /var/www/webdev.local && clear" -cur_console:s1TVna:t:"%finalValue%":C:"C:\cygwin64\Cygwin.ico" ^|^|^| ^
%kitty% -load 192.168.33.10 -cmd "cd /var/www/webdev.local && clear && echo 'Waiting for services (5 sec.)' && sleep 5 && npm run core:start" -cur_console:s1THna:t:"%finalValue%":C:"C:\cygwin64\Cygwin.ico" ^|^|^| ^
%kitty% -load 192.168.33.10 -cmd "cd /var/www/webdev.local && clear && sudo mc" -cur_console:s2THna:t:"%finalValue%":C:"C:\cygwin64\Cygwin.ico"
