#!/bin/sh

echo '--- [1] Install mc'
sudo apt-get --assume-yes install mc

echo '--- [2] Update node.js & npm'
sudo npm cache clean -f
sudo npm install -g n
sudo n stable

echo '--- [3] Update hosts'
vmip=192.168.33.10
for vhFile in /var/tmp/vagrant/hosts/*.conf
do
    sudo cp /var/tmp/vagrant/hosts/*.conf /etc/apache2/sites-available/ -R
    vhConf=${vhFile##*/}
    sudo a2ensite ${vhConf}
    vhost=${vhConf%.*}
    sudo sed -i "2i${vmip}    ${vhost}" /etc/hosts
done
sudo service apache2 restart

echo '--- [4] add "git.logic-games.spb.ru" to "/etc/hosts"'
sudo sed -i "192.168.250.11	git.logic-games.spb.ru" /etc/hosts

echo '--- ALL DONE ---'
