#!/bin/sh

echo 'Clone/install v6-repos:'
rootDir=$PWD

echo "--- [1] v6-core: git clone"
git clone https://bitbucket.org/v6team/v6-core.git $rootDir/repos/v6-core
if [ -f $rootDir/repos/v6-core/backend/package.json ]; then
	cd $rootDir/repos/v6-core/backend
	echo "--- v6-core: npm install"
	npm install
else
    echo "--- v6-core: error ('package.json' not found)"
	exit 1
fi

echo "--- [2] v6-services: git clone"
git clone https://bitbucket.org/v6team/v6-services.git $rootDir/repos/v6-services
if [ -f $rootDir/repos/v6-services/package.json ]; then
	cd $rootDir/repos/v6-services
	echo "--- v6-services: npm install"
	npm install
else
    echo "--- v6-services: error ('package.json' not found)"
	exit 1
fi

echo "--- [3] v6-template: git clone"
git clone https://bitbucket.org/v6team/v6-template.git $rootDir/repos/v6-template
if [ -f $rootDir/repos/v6-template/package.json ]; then
	cd $rootDir/repos/v6-template
	echo "--- v6-template: npm install"
	npm install
else
    echo "--- v6-template: error ('package.json' not found)"
	exit 1
fi

echo "--- [4] v6-apps: git clone"
git clone https://bitbucket.org/v6team/v6-apps.git $rootDir/repos/v6-apps
if [ -f $rootDir/repos/v6-apps/package.json ]; then
	cd $rootDir/repos/v6-apps
	echo "--- v6-apps: npm install"
	npm install
else
    echo "--- v6-apps: warn ('package.json' not found)"	
fi
