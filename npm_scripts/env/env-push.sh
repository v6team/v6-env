#!/bin/sh

echo "--- env:push"
rootDir=$PWD
cd $rootDir

echo "Comment: "
read comment

git add --all
git commit -am "$comment"
git push origin master

echo "--- done."
