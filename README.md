# v6-env

## Usage

### Start environment:
```
git clone git@git.logic-games.spb.ru:logic-games/v6-env.git
cd v6-env
npm run env:start
```

### SSH:
host: 192.168.33.10
user: vagrant
pass: vagrant

## Install
open `/var/www/webdev.local` from ssh and: 
`npm install`


### Windows hosts file:
`192.168.33.10	webdev.local`

### Working url:
http://webdev.local

### Commands:
see "scripts" from "package.json".

### Start:
npm run services:start
npm run core:start
